package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.ITaskRepository;
import com.t1.alieva.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {
    private final List<Task> tasks = new ArrayList<>();


    @Override
    public List<Task> findAll() {
        return tasks;
    }


    @Override
    public  Task add(final Task task){
        tasks.add(task);
        return task;
    }

    @Override
    public void clear(){
        tasks.clear();
    }
}
