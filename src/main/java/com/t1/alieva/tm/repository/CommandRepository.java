package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.ICommandRepository;
import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.model.Command;
import com.t1.alieva.tm.model.Task;

public final class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands."
    );

    public final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Display project list.");

    private final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task.");
    private final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Display task list.");

    private final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project.");

    public final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show information about device."
    );
    private final  static Command[] TERMINAL_COMMANDS = new Command[] {
            INFO, ABOUT, VERSION,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            HELP, EXIT
    };

    @Override
    public Command[] getTerminalCommands(){
        return TERMINAL_COMMANDS;
    }
}
