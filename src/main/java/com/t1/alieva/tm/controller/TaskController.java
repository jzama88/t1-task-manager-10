package com.t1.alieva.tm.controller;


import com.t1.alieva.tm.api.ITaskController;
import com.t1.alieva.tm.api.ITaskService;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.service.TaskService;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }
    @Override
    public void createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if(task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]"); }

    @Override
    public void clearTask(){
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTasks(){
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for(final Task task:tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
}
}
