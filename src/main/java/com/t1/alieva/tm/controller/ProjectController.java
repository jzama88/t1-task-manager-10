package com.t1.alieva.tm.controller;

import com.t1.alieva.tm.api.IProjectController;
import com.t1.alieva.tm.api.IProjectService;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if(project == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProject(){
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjects(){
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for(final Project project:projects){
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }
}
