package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.IProjectRepository;
import com.t1.alieva.tm.api.ITaskRepository;
import com.t1.alieva.tm.api.ITaskService;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {
    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final Task task) {
        if(task == null) return null;
        return taskRepository.add(task);
    }
    @Override
    public Task create(final String name){
        if(name == null || name.isEmpty()) return null;
        return add(new Task(name));
    }
    @Override
    public Task create(final String name, String description){
        if(name == null || name.isEmpty()) return null;
        if(description == null || description.isEmpty()) return null;
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }
}
