package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.*;
import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.controller.CommandController;
import com.t1.alieva.tm.controller.ProjectController;
import com.t1.alieva.tm.controller.TaskController;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.repository.CommandRepository;
import com.t1.alieva.tm.repository.ProjectRepository;
import com.t1.alieva.tm.repository.TaskRepository;
import com.t1.alieva.tm.service.CommandService;
import com.t1.alieva.tm.service.ProjectService;
import com.t1.alieva.tm.service.TaskService;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }

        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.TASK_LIST:
               taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String argument) {
        if (argument == null) {
            commandController.showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void run (final String[] args){
        if (processArguments(args)) System.exit(0);
        commandController.showWelcome();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }
}
