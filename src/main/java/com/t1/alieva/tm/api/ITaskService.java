package com.t1.alieva.tm.api;

import com.t1.alieva.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();

}
