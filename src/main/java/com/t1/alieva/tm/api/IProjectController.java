package com.t1.alieva.tm.api;

public interface IProjectController {

    void createProject();

    void clearProject();

    void showProjects();
}
