package com.t1.alieva.tm.api;

public interface ITaskController {

    void createTask();

    void clearTask();

    void showTasks();

}
