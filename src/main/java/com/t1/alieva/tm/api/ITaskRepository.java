package com.t1.alieva.tm.api;

import com.t1.alieva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void clear();

}
