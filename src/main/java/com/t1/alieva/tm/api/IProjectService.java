package com.t1.alieva.tm.api;

import com.t1.alieva.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(Project project);

    Project create (String name);

    Project create (String name, String description);

    void clear();

}
