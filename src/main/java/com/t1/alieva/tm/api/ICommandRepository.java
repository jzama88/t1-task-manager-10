package com.t1.alieva.tm.api;

import com.t1.alieva.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
